/* Created by O.V.Horyn on May, 2016
 */

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    rimraf = require('rimraf'), // The UNIX command rm -rf for node
    htmlmin = require('gulp-htmlmin'),
    postcss = require('gulp-postcss'),
    sass = require('gulp-sass'),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano')({
        reduceIdents: false }), // don't renames at-rules such as @keyframes. - 'http://cssnano.co/optimisations/#reduceidents'
    uglify = require('gulp-uglify'), // plugin for js  minifing
    pump = require('pump'); // plugin for gulp-uglify

var path = {
    build: { // Path of result files
        html: 'build/',
        js: 'build/js/',
        css: 'build/style/',
        img: 'build/img/',
        fonts: 'build/fonts/'
    },
    src: { // Path of source files
        html: 'src/*.html',
        js: 'src/js/*.js',
        style: 'src/style/style.sass',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: { // Path for watch changes
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/style/**/*.sass',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build' // Path for cleaning
};


/*
 * minify .html
 */
gulp.task('minify html', function() {
    return gulp.src( path.src.html )
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest( path.build.html ))
});


/*
 * CSS tasks:
 * 1. convert .sass to .css
 * 2. add vendor prefixes to .css
 * 3. minify .css
 */
gulp.task('css', function () {
    var processors = [
        autoprefixer,
        cssnano
    ];
    return gulp.src(path.src.style)
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(gulp.dest(path.build.css));
});


/*
 * minify .js
 */
gulp.task('minify js', function (cb) {
    pump([
            gulp.src( path.src.js ),
            uglify(),
            gulp.dest( path.build.js )
        ],
        cb
    );
})


/*
 * copy fonts to "build" directory
 */
gulp.task('fonts build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});


/*
 * copy fonts to "build" directory
 */
gulp.task('image build', function() {
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
});


/*
 * clean "build" directory
 * */
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});


/*
 * the task group runing
 * */
gulp.task('build', [
    'minify html',
    'css',
    'minify js',
    'fonts build',
    'image build',
]);


/*
 * files watcher
 * */
gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('minify html');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('css');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('minify js');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts build');
    });
});

/*
 * default task that will run all of our build
 * */
gulp.task('default', ['build', 'watch']);